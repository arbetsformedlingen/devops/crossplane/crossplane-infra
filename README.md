# Crossplane infrastructure for JobTech

This repo add the capability to a OCP cluster to configure resources
within AWS.

Currently at JobTech the following resources are allowed to manage:

* s3 buckets
* adding DNS records in jobtechdev.se.

Installed provider can manage other resources to but the AWS user
account used by crossplane do not have the permissions for other
resources.

## Usage

Before using this remember that Kubernetes is asynkron and
it can take a while before it have talked to AWS to create
requested resources.

Do not mix management resources between AWS console and kubernetes.
If you decide to manage a S3 bucket from Kubernetes, all actions
including deletion must be taken within Kubernetes.

### Create an S3 bucket

Decide the name of the desired bucket. Remember that S3 bucket names
must be globally unique. This means, for instance, only one cluster
can create a bucket with a given name.

Activate your desired namespace/project in kubernetes: `oc project my-project`

Create a YAML file describing your bucket. The following file
represents a bucket called `example-crossplane-bucket` in the
eu-north-1 region.

```yaml
apiVersion: s3.aws.jet.crossplane.io/v1alpha2
kind: Bucket
metadata:
  name: example-crossplane-bucket
  namespace: crossplane-system
spec:
  forProvider:
    region: eu-north-1
  providerConfigRef:
    name: default
```

There are more parameters under `forProvider`, see documentation for
the
[provider](https://doc.crds.dev/github.com/crossplane-contrib/provider-jet-aws/s3.aws.jet.crossplane.io/Bucket/v1alpha2@v0.4.2).
It is also valuable to look in the corresponding
[Terraform documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket).
The provider uses Terraform underneath.

Apply your YAML file: `kubectl apply -f my-file.yaml`

Check the status of the bucket with:
`kubectl get bucket.s3.aws.jet.crossplane.io`. It is ready when ready and synced column are true.

### Create a DNS record

To simplify usage of connecting DNS records to the correct load
balancer and ingress controller within Kubernetes there is a special
resource just doing that.

Activate your desired namespace/project in kubernetes: `oc project my-project`

Create a YAML file describing your bucket.

```yaml
apiVersion: crossplane.jobtechdev.se/v1alpha1
kind: DnsRecord
metadata:
  name: test-jobtechdev-se
spec:
  parameters:
    name: test
    domain: jobtechdev.se
```

When applying this a DNS record for `test.jobtechdev.se` will be
created and point to the route with the field
`host: test.jobtechdev.se`.

For a DNS record `test.api.jobtechdev.se` set the domain field in
the YAML to `api.jobtechdev.se`.

Apply your YAML file: `kubectl apply -f my-file.yaml`

Check the status of the DNS record with:
`kubectl get dnsrecord.crossplane.jobtechdev.se`. Once ready is true
the record has been created.

## Repo structure

The helm directory contains a values file to install the helm chart
for crossplane on a OpenShift cluster.

The kustomize directory contains settings to deploy crossplane-providers
and Jobtech compositions on an OpenShift cluster after above mentioned
helm chart is installed.

The examples directory contains some example resources.
