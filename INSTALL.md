# Installation instructions

## Preparations

Create a user accoount for crossplane on AWS. There shall be one per
cluster. Name it `svc-crossplane-test` where last part is cluster name. Give it only programmatic access.

Create a credentials.conf file like following and fill in the credentials:

```ini
[default]
aws_access_key_id = Access Key ID value
aws_secret_access_key = Secret access key value
```

Create a project crossplane-system and set upp secrets for AWS:

```shell
oc new-project crossplane-system
kubectl create secret generic aws-creds -n crossplane-system --from-file=creds=./credentials.conf
```

## Install crossplane helm

This section installs crossplane helm chart containing the base
package of crossplane.

```shell
helm repo add crossplane-stable https://charts.crossplane.io/stable
helm repo update

helm install crossplane --namespace crossplane-system crossplane-stable/crossplane --values helm/values.yaml
```

Verify everything looks okej:

```shell
helm list -n crossplane-system
kubectl get all -n crossplane-system
```

## Install kustomize files

This section installs:

* aws-terrajet provider to be able to configure AWS
* jobtech-configuration, containing composite resources to create AWS resources.

```shell
kubectl apply -f kustomize/base/aws-jet-provider.yaml
kubectl apply -f kustomize/base/ocp-controllerconfig.yaml

# Wait until provider is installed and healty:
kubectl get provider.pkg.crossplane.io -w

kubectl apply -k kustomize/clusters/test  # Change last part to cluster name
```
